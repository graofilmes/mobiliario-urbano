import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/videos/:path?',
    name: 'Videos',
    component: () => import('../views/Videos.vue')
  },
  {
    path: '/creditos/:path?',
    name: 'Creditos',
    component: () => import('../views/Creditos.vue')
  },
  {
    path: '/sobre/:path?',
    name: 'Sobre',
    component: () => import('../views/Sobre.vue')
  },
  {
    path: '/mais/:path?',
    name: 'Mais',
    component: () => import('../views/Mais.vue')
  },
  {
    path: '/:path?',
    name: 'Home',
    component: () => import('../views/Home.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router
