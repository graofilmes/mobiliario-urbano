const tags = {
    "BERGMAN 01": ["Abertura", "Banqueamento", "Escravidão", "Imigração", "História Oficial", "Potências", "Progresso", "Abolição", "Café", "Trabalho", "Discurso", "Poder Público", "Elite", "Negros", "Dispersão", "São Paulo", "Migração"],
    "BERGMAN 02": ["Código de posturas", "Criminalização", "Imigração", "História Oficial", "Progresso", "Trabalho", "Cultura Popular", "Especulação", "Discurso", "Elite", "População Vulnerável", "Negros", "Dispersão", "Violência", "Segregação", "São Paulo", "Migração"],
    "BERGMAN 03": ["Banqueamento", "Código de posturas", "Criminalização", "Imigração", "Discurso", "Poder Público", "Negros", "Violência", "Apagamento", "Morte", "Polícia", "São Paulo"],
    "BERGMAN 04": ["Banqueamento", "Código de posturas", "Criminalização", "Escravidão", "Imigração", "Discriminação", "Abolição", "Trabalho", "Poder Público", "Elite", "Negros", "São Paulo"],
    "BERGMAN 05": ["Criminalização", "Escravidão", "Imigração", "Trajetória histórica", "Trabalho", "Elite"],
    "BERGMAN 06": ["Imigração", "Trajetória histórica", "Discriminação", "Rua", "População Vulnerável", "Negros", "Mulheres", "Saúde", "Segregação", "Polícia"],
    "BERGMAN 07": ["Criminalização", "Trajetória histórica", "Elite", "Palácio dos Bandeirantes", "Ressignificar", "Mulheres", "Segregação"],
    "BERGMAN 08": ["Centro", "Periferia", "Centro Cultural", "Elite", "Baixa Renda", "Gentrificação", "Violência", "Segregação", "Polícia", "São Paulo", "Ocupação"],
    "BERGMAN 09": ["Fechamento"],
    "BERGMAN 10": ["Trajetória histórica", "História Oficial", "História Autônoma", "Potências", "Rua", "Quilombo", "Discurso", "Elite", "Violência", "Morte"],
    "CASE 01": ["Abertura", "Criminalização", "História Autônoma", "Igreja", "Resistência", "Território", "Indígena"],
    "CASE 02": ["História Oficial", "Manipulação", "Feudo", "Colonização", "Igreja", "Resistência", "Indígena", "Conflito", "São Paulo"],
    "CASE 03": ["Litoral", "Crise Hídrica", "Rio", "Ecologia", "Anhangabaú", "Bela Vista", "Poder Público", "Elite", "Resistência", "São Paulo", "Rio canalizado"],
    "CASE 04": ["Banqueamento", "Imigração", "História Oficial", "Discriminação", "Progresso", "Café", "Trabalho", "Indígena", "Higienização"],
    "CASE 05": ["Banqueamento", "História Oficial", "Rua", "Centro", "Litoral", "Jabaquara", "Liberdade", "Comercio de rua", "Cultura Popular", "Poder Público", "Elite", "Baixa Renda", "População Vulnerável", "Resistência", "Negros", "Território", "Despejo", "Indígena", "Higienização"],
    "CASE 06": ["Centro", "Periferia", "Rio", "Elite", "Baixa Renda", "Expansão", "Segregação", "São Paulo", "Higienização"],
    "CASE 07": ["História Oficial", "Nossa Senhora do Rosário", "Bolsa de Valores", "Cultura Popular", "Imposição de cultura", "Igreja", "Poder Público", "Baixa Renda", "População Vulnerável"],
    "CASE 08": ["Banqueamento", "Código de posturas", "Trajetória histórica", "História Autônoma", "Memória", "Oralidade", "Igreja", "Ancestralidade", "Poder Público", "Baixa Renda", "Negros", "Palácio dos Bandeirantes", "Indígena", "Degradação", "Morte", "Polícia", "Higienização"],
    "CASE 09": ["Código de posturas", "Criminalização", "Rua", "Periferia", "Cultura Popular", "Elite", "Baixa Renda", "Negros", "Indígena", "Significação", "Segregação"],
    "CASE 10": ["Financiamento", "Interesse", "Discriminação", "Comercio de rua", "Cultura Popular", "Preservação", "Elite", "Território", "Significação", "Ressignificar"],
    "CASE 11": ["Criminalização", "Financiamento", "Interesse", "Potências", "Nossa Senhora do Rosário", "Imposição de cultura", "Negros", "Indígena"],
    "CASE 12": ["Ecologia", "Poder Público", "Resistência", "Território", "Apropriação", "Reconstrução"],
    "CASE 13": ["Fechamento"],
    "CASE 14": ["Nossa Senhora do Rosário", "Banheiro Público", "Comercio de rua", "Igreja", "Poder Público", "Baixa Renda", "Segregação", "São Paulo"],
    "CASE 15": ["Banqueamento", "Kassab", "Poder Público", "Baixa Renda", "Negros", "Despejo", "Indígena", "Ressignificar", "Apagamento", "Morte"],
    "DANIEL 01": ["Abertura", "Necessidade", "Centro", "Periferia", "Trabalho", "Aluguel", "Elite", "Baixa Renda", "População Vulnerável", "São Paulo", "Ocupação", "Migração"],
    "DANIEL 02": ["Criminalização", "Privatização", "Empreiteiras", "Parcerias Público Privadas", "Trabalho", "Especulação", "Discurso", "Poder Público", "Banco", "Elite", "Baixa Renda", "Capitalismo", "População Vulnerável", "Moradia", "Território", "Ressignificar", "Agenciamento de corpos", "Ocupação"],
    "DANIEL 03": ["Fechamento"],
    "DANIEL 04": ["Laços Comunitários", "Moradia", "Significação", "Saúde", "Ocupação", "Mutirão"],
    "DANIEL 05": ["Criminalização", "Direitos", "Propriedade", "Periferia", "Aluguel", "Poder Público", "Elite", "População Vulnerável", "Despejo", "Degradação", "Violência", "Ocupação"],
    "DANIEL 06": ["Direitos", "Necessidade", "Centro", "Trabalho", "Especulação", "Elite", "Laços Comunitários", "Baixa Renda", "Resistência", "Conflito", "Dispersão", "Expansão"],
    "DANIEL 07": ["Acolhimento", "Diálogo", "Centro", "Periferia", "Trabalho", "Especulação", "Poder Público", "Elite", "Baixa Renda", "Capitalismo", "Moradia", "Agenciamento de corpos", "Dispersão", "Degradação", "São Paulo"],
    "DANIEL 08": ["Direitos", "Propriedade", "Necessidade", "Especulação", "Poder Público", "Resistência", "Moradia", "Território", "Conflito", "Ocupação"],
    "DANIEL 09": ["Financiamento", "Privatização", "Propriedade", "Centro", "Feudo", "Poder Público", "Baixa Renda", "População Vulnerável", "Resistência", "Conflito", "Segregação"],
    "DANIEL 10": ["Propriedade", "Discurso", "Banco", "Território", "Despejo", "Ocupação"],
    "DANIEL 11": ["Criminalização", "Trabalho", "População Vulnerável", "Resistência", "Moradia", "Despejo", "Significação", "Mulheres", "Ocupação"],
    "DIGA 01": ["Abertura", "Bela Vista", "Gentrificação"],
    "DIGA 02": ["Imigração", "Rio", "Bela Vista", "Aluguel", "Preservação", "Gentrificação", "Moradia", "Conflito", "Rio canalizado"],
    "DIGA 03": ["História Oficial", "História Autônoma", "Bela Vista", "Aluguel", "Imposição de cultura", "Especulação", "Discurso", "Agenciamento de corpos", "Apagamento", "Apropriação", "Segregação"],
    "DIGA 04": ["Trajetória histórica", "História Autônoma", "Memória", "Interesse", "Manipulação", "Centro Cultural", "Cultura Popular", "Imposição de cultura", "Discurso", "Laços Comunitários", "Gentrificação", "Moradia", "Ressignificar", "Agenciamento de corpos", "Apagamento", "Apropriação"],
    "DIGA 05": ["História Oficial", "Centro Cultural", "Especulação", "Poder Público", "Gentrificação", "Despejo", "Apagamento"],
    "DIGA 06": ["Propriedade", "Bela Vista", "Preservação", "Poder Público", "Resistência", "Moradia", "Despejo"],
    "DIGA 07": ["Imigração", "Memória", "Direitos", "Propriedade", "Progresso", "Bela Vista", "Trabalho", "Aluguel", "Discurso", "Preservação", "Morte", "Reconstrução"],
    "DIGA 08": ["Escravidão", "Imigração", "História Autônoma", "Financiamento", "Privatização", "Propriedade", "Rio", "Bela Vista", "Quilombo", "Especulação", "Ancestralidade", "Poder Público", "Resistência", "Negros", "Indígena", "Ressignificar", "São Paulo", "Ocupação"],
    "DIGA 09": ["Acolhimento", "História Autônoma", "Centro Cultural", "Cultura Popular", "Poder Público", "Laços Comunitários", "Resistência", "Moradia", "Morte", "Ocupação", "Mutirão"],
    "DIGA 10": ["Memória", "Financiamento", "Privatização", "Parcerias Público Privadas", "Bela Vista", "Centro Cultural", "Especulação", "Gentrificação", "Despejo"],
    "DIGA 11": ["Colonização", "Bela Vista", "Discurso", "Preservação", "Poder Público", "Apagamento"],
    "DIGA 12": ["História Autônoma", "Empreiteiras", "Centro Cultural", "Imposição de cultura", "Discurso", "Poder Público", "Banco", "Despejo", "Agenciamento de corpos", "Apagamento", "Apropriação"],
    "DIGA 13": ["Financiamento", "Interesse", "Privatização", "Empreiteiras", "Parcerias Público Privadas", "Especulação", "Banco", "Capitalismo", "Agenciamento de corpos", "Apropriação", "Segregação"],
    "DIGA 14": ["Fechamento"],
    "DIGA 15": ["Criminalização", "História Autônoma", "Memória", "Diálogo", "Manipulação", "Propriedade", "Centro Cultural", "Preservação", "Kassab", "Haddad", "Banco", "Capitalismo", "Resistência", "Moradia", "Despejo", "Violência", "Apagamento", "Apropriação", "Polícia"],
    "DRAO 01": ["Abertura", "Trajetória histórica", "Rua", "Abolição", "Café", "Colonização", "Bela Vista", "Comercio de rua", "Igreja", "Laços Comunitários", "Baixa Renda", "Gentrificação", "Negros", "Território", "Indígena", "Morte", "Segregação", "São Paulo", "Migração"],
    "DRAO 02": ["Escravidão", "Financiamento", "Progresso", "Centro", "Nossa Senhora do Rosário", "Bolsa de Valores", "Bela Vista", "Trabalho", "Igreja", "Poder Público", "Laços Comunitários", "Baixa Renda", "Gentrificação", "Negros", "Território", "Apagamento", "Segregação"],
    "DRAO 03": ["Código de posturas", "Abolição", "Periferia", "Bela Vista", "Comercio de rua", "Cultura Popular", "Poder Público", "Laços Comunitários", "Baixa Renda", "Resistência", "Negros", "Território", "Dispersão", "Migração"],
    "DRAO 04": ["Banqueamento", "Imigração", "Rua", "Centro", "Poder Público", "Baixa Renda", "Gentrificação", "Resistência", "Despejo", "Dispersão", "Segregação", "Reconstrução"],
    "DRAO 05": ["História Autônoma", "Centro", "Jabaquara", "Quilombo", "Território"],
    "DRAO 06": ["Banqueamento", "Imigração", "Abolição", "Café", "Trabalho", "Cultura Popular", "Poder Público", "Gentrificação", "Capitalismo", "Negros", "Indígena", "São Paulo"],
    "DRAO 07": ["Luiz Gama", "Trajetória histórica", "Nossa Senhora do Rosário", "Igreja", "Resistência", "Negros", "Mulheres"],
    "DRAO 08": ["Escravidão", "Nossa Senhora do Rosário", "Cultura Popular", "Igreja", "Resistência", "Negros", "Significação"],
    "DRAO 09": ["Escravidão", "História Oficial", "Cultura Popular", "Igreja", "Ancestralidade", "Laços Comunitários", "Resistência", "Mulheres"],
    "DRAO 10": ["Criminalização", "Centro", "Poder Público", "Baixa Renda", "Resistência", "Moradia", "Negros", "Segregação", "Migração"],
    "DRAO 11": ["Imigração", "História Autônoma", "Memória", "Discriminação", "Progresso", "Anhangabaú", "Comercio de rua", "Trem", "Cultura Popular", "Poder Público", "Laços Comunitários", "Resistência", "Despejo", "Apagamento", "Morte"],
    "DRAO 12": ["Fechamento"],
    "DRAO 13": ["Criminalização", "Ideologia", "Discriminação", "Rua", "Centro", "Poder Público", "Gentrificação", "População Vulnerável", "Dispersão", "Violência", "Apagamento", "Morte", "Segregação", "Polícia", "São Paulo", "Higienização", "Migração"],
    "JULIO 01": ["Memória", "Jabaquara", "Preservação", "Ancestralidade", "Negros", "Morte"],
    "JULIO 02": ["Escravidão", "História Autônoma", "Necessidade", "Especulação", "Preservação", "Capitalismo", "Resistência", "Significação"],
    "JULIO 03": ["Escravidão", "História Autônoma", "Memória", "Progresso", "Periferia", "Trem", "Centro Cultural", "Especulação", "Banco", "Despejo", "Dispersão", "Apropriação"],
    "JULIO 04": ["História Oficial", "História Autônoma", "Memória", "Oralidade", "Ideologia", "Liberdade", "Preservação", "Capitalismo", "Negros", "Indígena", "Dispersão", "Segregação"],
    "JULIO 05": ["Cultura Popular", "Imposição de cultura", "Preservação", "Capitalismo", "Resistência", "Despejo", "Apagamento", "Segregação"],
    "JULIO 06": ["Abertura", "Imigração", "Trajetória histórica", "História Oficial", "História Autônoma", "Memória", "Potências", "Ecologia", "Cultura Popular", "Especulação", "Negros", "Território", "Despejo", "Indígena", "Apropriação", "São Paulo"],
    "JULIO 07": ["Progresso", "Litoral", "Rio", "Ecologia", "Igreja", "Preservação"],
    "JULIO 08": ["Luiz Gama", "História Autônoma", "Abolição", "Preservação", "Negros"],
    "JULIO 09": ["Escravidão", "História Oficial", "História Autônoma", "Oralidade", "Ideologia", "Propriedade", "Agenciamento de corpos", "Apagamento", "Morte"],
    "JULIO 10": ["Escravidão", "História Autônoma", "Privatização", "Propriedade", "Apagamento"],
    "JULIO 11": ["Escravidão", "Discriminação", "Progresso", "Abolição", "Litoral", "Rio", "Jabaquara", "Quilombo", "Laços Comunitários", "Resistência"],
    "JULIO 12": ["Fechamento"],
    "JULIO 13": ["Acolhimento", "Escravidão", "História Autônoma", "Memória", "Oralidade", "Abolição", "Igreja", "Preservação", "Significação"],
    "SABRINA 01": ["Abertura", "Interesse", "Empreiteiras", "Anhangabaú", "Discurso", "Kassab", "Haddad", "Poder Público", "Banco", "Elite", "Conflito"],
    "SABRINA 02": ["Ideologia", "Diálogo", "Financiamento", "Empreiteiras", "Especulação", "Kassab", "Haddad", "Poder Público", "Banco", "Gentrificação", "Significação", "Expansão"],
    "SABRINA 03": ["Criminalização", "Interesse", "Discriminação", "Centro", "Crise Hídrica", "Rio", "Anhangabaú", "Especulação", "Capitalismo", "População Vulnerável", "Negros", "Despejo", "Agenciamento de corpos", "Polícia"],
    "SABRINA 04": ["Financiamento", "Empreiteiras", "Parcerias Público Privadas", "Rio", "Comercio de rua", "Poder Público", "Banco", "Agenciamento de corpos", "Rio canalizado"],
    "SABRINA 05": ["Banheiro Público", "Comercio de rua", "Trabalho", "Discurso", "Poder Público", "Capitalismo", "População Vulnerável", "Despejo", "Dispersão", "Degradação", "São Paulo"],
    "SABRINA 06": ["Privatização", "Direitos", "Manipulação", "Discurso", "Banco", "População Vulnerável", "Apropriação", "Reconstrução"],
    "SABRINA 07": ["Trajetória histórica", "Privatização", "Propriedade", "Centro", "Empreiteiras", "Parcerias Público Privadas", "Especulação", "Poder Público", "Elite", "Capitalismo", "Despejo", "Expansão", "Violência", "Ocupação"],
    "SABRINA 08": ["Propriedade", "Especulação", "Gentrificação", "Capitalismo", "Ressignificar", "Degradação"],
    "SABRINA 09": ["Especulação", "Discurso", "Gentrificação", "Capitalismo", "Resistência", "Ressignificar", "Conflito", "Apropriação", "Higienização"],
    "SABRINA 10": ["Fechamento", "Trajetória histórica", "Diálogo", "Poder Público", "Banco", "Capitalismo", "População Vulnerável", "Agenciamento de corpos"],
    "SABRINA 11": ["Trabalho", "Despejo", "Conflito", "Agenciamento de corpos", "Expansão", "Violência", "Polícia"],
    "SABRINA 12": ["Interesse", "Necessidade", "Banheiro Público", "Especulação", "Capitalismo", "Violência"]
}

const weights = {}

Object.keys(tags).forEach(element => {
    weights[element] = {}
    Object.keys(tags).forEach(video => {
        if(video !== element) {
            common_tags_num = tags[element].filter(value => tags[video].includes(value))
            weights[element][video] = common_tags_num.length
        }
    })
})

Object.keys(weights).forEach(video => {
    let s = 0
    Object.values(weights[video]).forEach (tag => {
        s += tag === 0 ? 0 : 1
    })
    Object.keys(weights[video]).forEach (tag => {
        weights[video][tag] /= s
    })
})

console.log(weights)
